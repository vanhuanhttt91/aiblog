﻿namespace AIBlog.Data
{
    public class Neo4jSetting
    {
        public Neo4jSetting()
        {

        }
        public string BoltUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
