﻿using Neo4j.Driver.V1;
using System;
using System.Threading.Tasks;

namespace AIBlog.Repo
{
    public class TransactionManager : ITransactionManager
    {
        protected readonly IDriver Driver;
        private ISession _session;
        public TransactionManager(IDriver driver)
        {
            this.Driver = driver;
            this._session = driver.Session();
        }
        public ITransaction GetTransaction()
        {
            return this._session.BeginTransaction();
        }

        public async Task CommitAsync(ITransaction transaction)
        {
            transaction.Success();
            await transaction.CommitAsync();
        }

        public Task RollbackAsync(ITransaction transaction)
        {
            return transaction.RollbackAsync();
        }

        #region Dispose
        /// <summary>
        /// Releases all resources used by the WarrantManagement.DataExtract.Dal.ReportDataBase
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases all resources used by the WarrantManagement.DataExtract.Dal.ReportDataBase
        /// </summary>
        /// <param name="disposing">A boolean value indicating whether or not to dispose managed resources</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_session != null)
                {
                    _session.Dispose();
                }
            }

        }
        #endregion
    }
}
