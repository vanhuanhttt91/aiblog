﻿using AIBlog.Data;
using System.Threading.Tasks;

namespace AIBlog.Repo
{
    public interface IRepositoryUser
    {
        Task<bool> Insert(User user);
    }
}
