﻿using AIBlog.Data;
using AIBlog.Repo;
using AIBlog.Service;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Neo4j.Driver.V1;
using Neo4jClient;
using System.Reflection;
using System.Text;

namespace Web.AIBlog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.Configure<Neo4jSetting>(Configuration.GetSection("Neo4jSetting"));
            services.AddAutoMapper(Assembly.GetAssembly(typeof(MappingConfiguration)));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IRepositoryUser, RepositoryUser>();
            RegisterJWTAuthentication(services);
            services.AddMvc();
            RegisterNeo4jDriver(services);
            RegisterGraphClient(services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
        private IServiceCollection RegisterNeo4jDriver(IServiceCollection services)
        {
            services.AddSingleton(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4jSetting").Get<Neo4jSetting>();
                var authToken = AuthTokens.Basic(neo4jSetting.UserName, neo4jSetting.Password);
                var config = new Config();
                var driver = GraphDatabase.Driver(neo4jSetting.BoltUrl, authToken, config);
                return driver;
            });

            return services;
        }

        private IServiceCollection RegisterGraphClient(IServiceCollection services)
        {
            services.AddSingleton(typeof(IBoltGraphClient), resolver =>
            {
                var client = new BoltGraphClient(services.BuildServiceProvider().GetService<IDriver>());

                if (!client.IsConnected)
                {
                    client.Connect();
                }

                return client;
            });

            return services;
        }

        private IServiceCollection RegisterJWTAuthentication(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
           .AddJwtBearer(options =>
           {
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,

                   ValidIssuer = "http://localhost:44383",
                   ValidAudience = "http://localhost:44383",
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("JwtAuthentication").Get<Authentication>().HashKey))
               };
           });
            return services;
        }
    }
}
