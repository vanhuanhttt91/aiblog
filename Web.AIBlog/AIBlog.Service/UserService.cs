﻿using AIBlog.Data;
using AIBlog.Repo;
using System.Threading.Tasks;

namespace AIBlog.Service
{
    public class UserService : IUserService
    {
        private IRepositoryUser _repositoryUser;
        public UserService(IRepositoryUser repositoryUser)
        {
            _repositoryUser = repositoryUser;
        }

        public Task<bool> Insert(User user)
        {
            var repoUser = _repositoryUser.Insert(user);
            return repoUser;
        }

        
    }
}
