﻿using AIBlog.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AIBlog.Service
{
    public interface IUserService
    {
        Task<bool> Insert(User user);
    }
}
